package com.geoquiz.view.menu;

import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.button.MyButtonFactory;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;

public class BasicScene extends Scene {

    private static final double BACK_BUTTON_WIDTH = 350;
    private static final Color BACK_BUTTON_COLOR = Color.BLUE;
    final Stage mainStage;

    public BasicScene(final Stage mainStage) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());
        this.mainStage = mainStage;
    }

    protected Stage getMainStage(){
        return  this.mainStage;
    }

    protected MyButton createBackButton(){
        MyButton button = MyButtonFactory.createMyButton(Buttons.INDIETRO.toString(), BACK_BUTTON_COLOR, BACK_BUTTON_WIDTH);
        ((Node) button).setOnMouseClicked(event -> {
            if (!MainWindow.isWavDisabled()) {
                MainWindow.playClick();
            }
            try {
                this.getMainStage().setScene(new MainMenuScene(this.getMainStage()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return button;
    }
}
