package com.geoquiz.view.menu;

import com.geoquiz.view.utility.Background;

import java.io.IOException;
import java.util.Optional;

import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.ButtonsCategory;
import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.button.MyButtonFactory;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelFactory;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

//TODO: create superclass for all Scene
/**
 * The scene where user can choose game category.
 */
public class CategoryScene extends BasicScene {

    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 450;
    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_X_BACK = 450;
    private static final double POS_Y_BACK = 600;
    private static final double CATEGORY_BUTTON_WIDTH = 350;
    private static final Color CATEGORY_BUTTON_COLOR = Color.BLUE;
    private static final double USER_LABEL_FONT = 40;

    private final Pane panel = new Pane();
    private final HBox hbox = new HBox(10);
    private final HBox hbox2 = new HBox(10);
    private final VBox vbox = new VBox();

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public CategoryScene(final Stage mainStage) {
        super(mainStage);
        this.initializeLayout();
        this.initializeLabels();
        this.initializeButtons();
        this.setRoot(this.panel);
    }

    private void initializeLayout() {
        hbox.setTranslateX(POS_1_X);
        hbox.setTranslateY(POS_1_Y);
        hbox2.setTranslateX(POS_2_X);
        hbox2.setTranslateY(POS_2_Y);
        vbox.setTranslateX(POS_X_BACK);
        vbox.setTranslateY(POS_Y_BACK);
    }

    private void initializeButtons() {
        final MyButton capitals = this.createCategoryButton(ButtonsCategory.CAPITALI);
        final MyButton currencies = this.createCategoryButton(ButtonsCategory.VALUTE);
        final MyButton dishes = this.createCategoryButton(ButtonsCategory.CUCINA);
        final MyButton monuments = this.createCategoryButton(ButtonsCategory.MONUMENTI);
        final MyButton flags = this.createCategoryButton(ButtonsCategory.BANDIERE);
        final MyButton back = this.createBackButton();

        hbox.getChildren().addAll((Node) flags, (Node) currencies, (Node) dishes);
        hbox2.getChildren().addAll((Node) monuments, (Node) capitals);
        vbox.getChildren().add((Node) back);
    }

    private void initializeLabels() {
        final MyLabel userLabel = MyLabelFactory.createMyLabel("USER: " + LoginMenuScene.getUsername(), Color.BLACK,
                USER_LABEL_FONT);
        this.panel.getChildren().addAll(Background.getImage(), Background.createBackground(), hbox, hbox2, vbox,
                Background.getLogo(), (Node) userLabel);
    }

    protected MyButton createCategoryButton(final ButtonsCategory category){
        MyButton button = MyButtonFactory.createMyButton(category.toString(), CATEGORY_BUTTON_COLOR, CATEGORY_BUTTON_WIDTH);
        ((Node) button).setOnMouseClicked((event) -> {
            if (!MainWindow.isWavDisabled()) {
                MainWindow.playClick();
            }
            mainStage.setScene(new ModeScene(this.getMainStage(), category));
        });
        return  button;
    }

}
